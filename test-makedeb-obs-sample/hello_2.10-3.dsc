Format: 3.0 (quilt)
Source: hello
Binary: hello
Architecture: any
Version: 2.10-3
Maintainer: Santiago Vila <sanvila@debian.org>
Homepage: https://www.gnu.org/software/hello/
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/sanvila/hello
Vcs-Git: https://salsa.debian.org/sanvila/hello.git
Testsuite: autopkgtest
Build-Depends: debhelper-compat (= 13), help2man, texinfo
Package-List:
 hello deb devel optional arch=any
Checksums-Sha1:
 d3078deb6f472a9238bb006cabb9d616e1219756 520028 hello_2.10.orig.tar.xz
 a2d122fd090dbab3d40b219a237fbb7d74f8023a 12684 hello_2.10-3.debian.tar.xz
Checksums-Sha256:
 ff311dd3d7c41d0dedc9cb80a6eb3136a57f8ce9d836533717334cfbb45280c8 520028 hello_2.10.orig.tar.xz
 60ee7a466808301fbaa7fea2490b5e7a6d86f598956fb3e79c71b3295dc1f249 12684 hello_2.10-3.debian.tar.xz
Files:
 aee05c3b83f301f9bc0d01d03a6eb212 520028 hello_2.10.orig.tar.xz
 27ab798c1d8d9048ffc8127e9b8dbfca 12684 hello_2.10-3.debian.tar.xz
