#!/bin/bash

#
# AAT-DEBIAN-REPO AUTOMATED BUILD SCRIPT FOR SERVERS (CHROMIUM ONLY)
#
# The MIT License (MIT)
# Copyright © 2022 Antoni Aloy Torrens <aaloytorrens@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# https://mit-license.org/
#

RELEASE="bullseye-security"
FILE_LATEST_BUILD="latest-build.txt"

# Get latest version (the first one) from Chromium
# The first declaration is commented, it is faster but I need to have Chromium package installed
#CHROMIUM_UPSTREAM_VERSION=$(dpkg-query --show --showformat '${Version}' chromium)
CHROMIUM_UPSTREAM_VERSION=$(apt-cache madison chromium | grep $RELEASE | cut -d \| -f 2 | head -n1 | xargs)

# Exit on CTRL+C
trap "exit" INT

# Exit on any error
set -eu

# Packaging status (bullseye)
# Chromium -> Enables Hangouts / Google Chat screen sharing extension
# We have made a special script for Chromium, as this package has lots of updates and secfixes.
# Other packages are not that rellevant unless they are bugfix, but this is.

# Check if chromium needs to be built or not, according to version.
# Return 0: true
# Return 1: false
is_needed_build(){
	if [ -f $FILE_LATEST_BUILD ]; then
		# Get upstream version and compare to our latest build
		LOCAL_VERSION=$(cat $FILE_LATEST_BUILD)

		echo "Comparing Local Version with the Upstream one..."
		echo "LOCAL_VERSION: $LOCAL_VERSION; CHROMIUM_UPSTREAM_VERSION: ${CHROMIUM_UPSTREAM_VERSION}"
		NEEDS_BUILD=$(dpkg --compare-versions $LOCAL_VERSION lt $CHROMIUM_UPSTREAM_VERSION && echo "true" || echo "false")
		if [ $NEEDS_BUILD == "true" ]; then
			echo "Chromium needs building. Building Chromium from source..."
			return 0
		fi
		echo "Chromium does not need building. Done!"
		echo "FINISHED JOB ON $(date +'%Y-%m-%d %H:%M:%S')"
		return 1
	else
		echo "latest-build.txt does not exist. Building Chromium from source..."
		return 0
	fi
}

cd $HOME/chromium-build-check

# Report time and date
echo "STARTED JOB AT $(date +'%Y-%m-%d %H:%M:%S')"

# Update and cleanup
echo "Update and cleanup..."
sudo apt-get update >/dev/null; sudo apt-get -yq full-upgrade >/dev/null; sudo apt-get -y autoremove >/dev/null; sudo apt-get clean

# Check if is needed build
if ! is_needed_build; then
	exit 0
fi

# Retrieve package from security sources
rm -rf src_chromium
mkdir -p src_chromium
cd src_chromium
echo "Retrieving source, version $CHROMIUM_UPSTREAM_VERSION..."
apt-get -yq -t $RELEASE source chromium >/dev/null

# Rename chromium directory to chromium
echo "Renaming chromium-$CHROMIUM_UPSTREAM_VERSION to chromium..."
find . -maxdepth 1 -mindepth 1 -type d -exec mv {} chromium \;

# Enable Hangouts / Google Chat screen sharing extension
echo "Enabling Hangouts / Google Chat screen sharing extension..."
sed -i 's/enable_hangout_services_extension=false/enable_hangout_services_extension=true/g' chromium/debian/rules

# Move to obsbuilder
echo "Deleting all Chromium folders in obsbuilder, moving the new directory there..."
sudo rm -rf /home/obsbuilder/chromium*
sudo mv chromium /home/obsbuilder
sudo chown -R obsbuilder:obsbuilder /home/obsbuilder/chromium

# Remove source directory
cd ..
rm -rf src_chromium

# Build Debian package and then remove chromium source from directory
echo "Building package..."
build-security-debian-package-nogit chromium && echo "Success! Writing version to file..." && echo "$CHROMIUM_UPSTREAM_VERSION" > $FILE_LATEST_BUILD && sudo rm -rf /home/obsbuilder/chromium

echo "FINISHED JOB ON $(date +'%Y-%m-%d %H:%M:%S')"
