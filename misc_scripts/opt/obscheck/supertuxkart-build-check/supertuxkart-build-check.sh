#!/bin/bash

#
# AAT-DEBIAN-REPO AUTOMATED BUILD SCRIPT FOR SERVERS (SUPERTUXKART ONLY)
#
# The MIT License (MIT)
# Copyright © 2022 Antoni Aloy Torrens <aaloytorrens@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# https://mit-license.org/
#

RELEASE="sid"
FILE_LATEST_BUILD="latest-build.txt"

# Get latest version (the first one) from Supertuxkart
# The first declaration is commented, it is faster but I need to have Supertuxkart package installed
#STK_UPSTREAM_VERSION=$(dpkg-query --show --showformat '${Version}' supertuxkart)
STK_UPSTREAM_VERSION=$(apt-cache madison supertuxkart | grep sid | cut -d \| -f 2 | head -n1 | xargs)

# Exit on CTRL+C
trap "exit" INT

# Exit on any error
set -eu

# Packaging status (bullseye)
# Check if supertuxkart needs to be built or not, according to version.
# Return 0: true
# Return 1: false
is_needed_build(){
	if [ -f $FILE_LATEST_BUILD ]; then
		# Get upstream version and compare to our latest build
		LOCAL_VERSION=$(cat $FILE_LATEST_BUILD)

		echo "Comparing Local Version with the Upstream one..."
		echo "LOCAL_VERSION: $LOCAL_VERSION; STK_UPSTREAM_VERSION: ${STK_UPSTREAM_VERSION}"
		NEEDS_BUILD=$(dpkg --compare-versions $LOCAL_VERSION lt $STK_UPSTREAM_VERSION && echo "true" || echo "false")
		if [ $NEEDS_BUILD == "true" ]; then
			echo "SuperTuxKart needs building. Building SuperTuxKart from source..."
			return 0
		fi
		echo "SuperTuxKart does not need building. Done!"
		echo "FINISHED JOB ON $(date +'%Y-%m-%d %H:%M:%S')"
		return 1
	else
		echo "latest-build.txt does not exist. Building SuperTuxKart from source..."
		return 0
	fi
}

cd $HOME/supertuxkart-build-check

# Report time and date
echo "STARTED JOB AT $(date +'%Y-%m-%d %H:%M:%S')"

# Update and cleanup
echo "Update and cleanup..."
sudo apt-get update >/dev/null; sudo apt-get -yq full-upgrade >/dev/null; sudo apt-get -y autoremove >/dev/null; sudo apt-get clean

# Check if is needed build
if ! is_needed_build; then
	exit 0
fi

# Create patches directory
mkdir -p patches

# Retrieve package from security sources
rm -rf src_stk
mkdir -p src_stk
cd src_stk
echo "Retrieving source, version $STK_UPSTREAM_VERSION..."
apt-get -yq -t $RELEASE source supertuxkart >/dev/null

# Rename chromium directory to chromium
echo "Renaming supertuxkart-$STK_UPSTREAM_VERSION to supertuxkart..."
find . -maxdepth 1 -mindepth 1 -type d -exec mv {} supertuxkart \;

# Do not use Vulkan. Library is too old
echo "Tweaking SuperTuxKart, do not use Vulkan library, as it is too old"
if [ ! -f "patches/remove-shaderc.patch" ]; then
	wget -q https://github.com/supertuxkart/stk-code/compare/1.4...292bafcf9a35435e1d2f0acc196625f6234fcaa2.patch -O ../patches/remove-shaderc.patch
fi

# Allow errors here
set +eu
patch --force -p1 -d supertuxkart < ../patches/remove-shaderc.patch >/dev/null
set -eu

# Build STK without SHADERC
sed -i "s/-DUSE_CRYPTO_OPENSSL=OFF/-DUSE_CRYPTO_OPENSSL=OFF -DNO_SHADERC=ON/g" supertuxkart/debian/rules

# Move to obsbuilder
echo "Deleting all SuperTuxKart folders in obsbuilder, moving the new directory there..."
sudo rm -rf /home/obsbuilder/supertuxkart*
sudo mv supertuxkart /home/obsbuilder
sudo chown -R obsbuilder:obsbuilder /home/obsbuilder/supertuxkart

# Remove source directory
cd ..
rm -rf src_stk

# Build Debian package and remove SuperTuxKart from directory
echo "Building package..."
build-debian-package-nogit supertuxkart && echo "Success! Writing version to file..." && echo "$STK_UPSTREAM_VERSION" > $FILE_LATEST_BUILD && sudo rm -rf /home/obsbuilder/supertuxkart

echo "FINISHED JOB ON $(date +'%Y-%m-%d %H:%M:%S')"
