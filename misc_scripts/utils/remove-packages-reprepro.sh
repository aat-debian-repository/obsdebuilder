#!/bin/sh
cd /var/www/repository/debian
reprepro list codename | awk '{print $2}' > packages_list.txt

while read -r package; do
    reprepro remove codename "$package"
done < packages_list.txt

