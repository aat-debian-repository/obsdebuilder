#!/bin/bash

# loop through each line of debian/patches/series file
while read line; do
  # check if the line does not start with "#"
  if [[ "$line" != \#* ]]; then
    # apply the patch using "patch" command
    patch -p1 < debian/patches/"$line"
  fi
done < debian/patches/series
