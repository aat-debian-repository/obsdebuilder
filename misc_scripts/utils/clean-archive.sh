#!/bin/sh
cd "$(dirname "$0")"

# Clean OBS built packages
rm -rf *-obs

# Clean OBS updated packages
rm -rf *-update

# Clean local repo
rm -rf /usr/lib/local-apt-repository-*
rm -rf /srv/local-apt-repository-*

# Clean logs and tmp files
rm -rf tmp/*
echo "" > log/log.txt
rm -rf __pycache__

# Clean .deb files in system root
rm -rf /*.deb
