from json import loads
from prettytable import PrettyTable
from shutil import chown
from pathlib import Path

def show_summary(user, summary_log_file, logger):
    summary = []

    # Misc operations to summary_file (delete, create and set permissions)
    summary_file = "summary.txt"
    Path(summary_file).unlink(missing_ok=True)
    with open(summary_file, mode="w", encoding="utf-8") as f:
        f.write("")
    chown(summary_file, user=user, group=user)

    with open(summary_log_file, mode="r", encoding="utf-8") as f:
        data = f.readlines()
        for report in data:
            summary.append(report.strip())
    write_to_summary(summary, logger, summary_file)

def print_summary(summary, logger, file):
    # Print prettified table (python3-prettytable)
    table = [['STATUS', 'BUILT', 'SKIPPED', 'FAILED']]
    tab = PrettyTable(table[0])

    # Package status count
    logger.info(f"")
    logger.info(f"SUMMARY")
    logger.info(f"=======")
    built = 0
    skip = 0
    failed = 0
    for pkg in summary:
        pkg = loads(pkg)
        status = pkg["package"][5]
        built += (1) if (status == "BUILT") else 0
        skip += (1) if (status == "SKIP") else 0
        failed += (1) if (status == "FAILED") else 0
    tab.add_row(["SUMMARY", built, skip, failed])

    print_table = tab.get_string()
    rows = tab.get_string().split("\n")
    for row in rows:
        logger.info(row)

    with open(file, mode="a", encoding="utf-8") as f:
        f.write(f"SUMMARY\n")
        f.write(f"=======\n")
        f.write(print_table)

def print_details(summary, logger, file):
    # Print prettified table (python3-prettytable)
    table = [['Package', 'Arch', 'Version', 'Config', 'Status']]
    tab = PrettyTable(table[0])

    # Details of each package
    logger.info(f"")
    logger.info(f"")
    logger.info(f"DETAILS")
    logger.info(f"=======")
    for pkg in summary:
        pkg = loads(pkg)
        package = pkg["package"][0]
        arch = pkg["package"][1]
        version = pkg["package"][2]
        config = pkg["package"][3]
        distribution = pkg["package"][4]
        status = pkg["package"][5]

        details = f"Package {package}, version {version}, {config} ({distribution}, {arch}) => {status}"
        tab.add_row([package, arch, version, f"{config} ({distribution}, {arch})", status])

        print_table = tab.get_string()
        rows = tab.get_string().split("\n")

    for row in rows:
        logger.info(row)

    with open(file, mode="a", encoding="utf-8") as f:
        f.write("\n")
        f.write("\n")
        f.write(f"DETAILS\n")
        f.write(f"=======\n")
        f.write(print_table)
        f.write("\n")


def write_to_summary(summary, logger, write_to_file):
    print_summary(summary, logger, write_to_file)
    print_details(summary, logger, write_to_file)
