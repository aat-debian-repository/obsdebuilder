#!/usr/bin/env python3
# This script will run inside chroot

### logger.py
# https://github.com/antonialoytorrens-ikaue/simple-pylogging

from datetime import time
import logging

class Logger:
    """
    Simple logger that allows you to write in different locations
    without losing any console output.

    # Declare logger
    logger_init = Logger()

    # Init logger
    logger_init.set_log_filename(f"log/log_123.log")
    logger = logger_init.get_logger()

    # Set message levels in logger
    logger.info("INFO message")
    logger.debug("DEBUG message")
    logger.warning("WARNING message")
    logger.error("ERROR message")
    """
    def __init__(self, log_filename: str = None):
        # Check if log_filename is not empty
        self.log_filename = "log.txt"

        if(not check_null_or_empty(log_filename)):
            self.log_filename = log_filename

        # * Init the logger
        # Create a custom logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        # Create a formatter for the handlers
        log_format = "%(asctime)s,%(msecs)03d (%(levelname)5s) [%(filename)s:%(lineno)d]: %(message)s"
        self.__formatter = logging.Formatter(log_format, datefmt="%d-%m-%Y %H:%M:%S")

        # Create handlers
        self._create_handlers()

    def _create_handlers(self):
        # Clear all handlers
        # self.logger.handlers.clear()

        self.__create_console_handler()
        self.__create_file_handler()

    def __create_console_handler(self):
        # Clear ConsoleHandler if it is set
        """
        try:
            # [0] corresponds to ConsoleHandler
            self.logger.removeHandler(self.logger.handlers[0])
        except IndexError:
            pass
        """

        # Create handler
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.INFO)

        # Add formatter to handler
        console_handler.setFormatter(self.__formatter)

        # Add handler to the logger
        self.logger.addHandler(console_handler)

    def __create_file_handler(self):
        # Clear FileHandler if it is set
        try:
            # [1] corresponds to FileHandler
            self.logger.removeHandler(self.logger.handlers[1])
        except IndexError:
            pass

        # Create handler
        file_handler = logging.FileHandler(self.log_filename, mode="a", encoding="utf-8")
        file_handler.setLevel(logging.DEBUG)

        # Add formatter to handler
        file_handler.setFormatter(self.__formatter)

        # Add handler to the logger
        self.logger.addHandler(file_handler)

    def set_log_filename(self, filename):
        if(not(check_null_or_empty(filename))):
            self.log_filename = filename
        else:
            print("LOGGER WARNING: Log filename must not be null or empty. Defaulting to log.txt.")
            self.log_filename = "log.txt"

        # FileHandler is removed, we don't want to have two log outputs at the same time
        self.__create_file_handler()

    def get_logger(self):
        return self.logger

def check_null_or_empty(msg: str):
    return not msg or msg.isspace()

### End logger.py

from os import chdir
from shutil import copy
from glob import glob
from pathlib import Path
import subprocess
import argparse

def update_repositories(logger_init):
    subprocess.check_output(f"sudo apt-get update --allow-unauthenticated >>{logger_init.log_filename} 2>&1", shell=True, timeout=1800)

def fix_ownership(name, logger_init):
    subprocess.check_output(f"sudo chown -R abuild:abuild {name}", shell=True, timeout=1800)

def install_makedeb(logger_init):
    makedeb_release = "export MAKEDEB_RELEASE='makedeb'"
    #subprocess.check_output(f"{makedeb_release}; wget -qO - 'https://shlink.makedeb.org/install' | bash", shell=True, stderr=subprocess.DEVNULL, timeout=1800)
    subprocess.check_output(f"MAKEDEB_RELEASE='makedeb' sudo apt-get -yq -f install ./makedeb_16.0.0-stable_all.deb", shell=True, stderr=subprocess.DEVNULL, timeout=1800)

def build_package(package, logger_init):
    # Get inside folder
    chdir(Path(package).absolute())

    # Build (48h timeout)
    subprocess.check_output(f"(yes | makedeb -sr) >> ../{logger_init.log_filename} 2>&1", shell=True, timeout=172800)

if __name__ == "__main__":
    # Declare logger
    logger_init = Logger()

    # Init logger
    logger_init.set_log_filename(f"log.txt")
    logger = logger_init.get_logger()

    # Argparser
    aparser = argparse.ArgumentParser(description="Script that builds makedeb packages inside chroot. Note: the PKGBUILD must be in $HOME/folder, equal to the name of the package.")
    aparser.add_argument("--name", type=str, required="True", help="Name of package")

    aparser_arguments = aparser.parse_args()

    # Name
    if aparser_arguments.name:
        name = aparser_arguments.name
    else:
        logger.error("Package name not provided in script!")

    # Info
    logger.info("Executing Makedeb builder script...")

    # Update packages
    logger.info("Updating repositories...")
    update_repositories(logger_init)

    # Fix ownership (package has root ownership)
    logger.info("Fixing permissions...")
    fix_ownership(name, logger_init)

    # Install makedeb
    logger.info(f"Installing makedeb...")
    install_makedeb(logger_init)

    # Build package
    logger.info(f"Building makedeb package {name} in chroot...")
    build_package(name, logger_init)

    # Success
    logger.info(f"Built makedeb {name} successfully!")

    # Copy package to deb location
    for pkg in glob("*.deb"):
        copy(pkg, Path("/usr/src/packages/DEBS/").absolute())
