#!/usr/bin/env python3

"""
This file aims to provide an easy implementation about Debian packaging on Open Build Service, aka OBS.
According to the wiki, there are two files needed:
*    The pristine tarball (with format $PKGNAME_$PKGVER.tar.xz)
*    A .dsc file (with format $PKGNAME_$PKGVER.dsc)

Assuming the packages are already uploaded into Git (with included debian/ subdirectory), this script does the following:
* Set up a chroot of a specified arch. If the arch is not native, it runs in QEMU.
* Downloads the source Git package in chroot.
* Builds and signs the package (debuild -S).
* Removes the source git package and downloads it again (so it forces cleaning src).

* Makes pristine tarball$PKGNAME_$PKGVER.tar.xz file.
* Saves generated *.dsc file.

* Upload to OBS.
* Generates Debian repository (reprepro).
"""

# TODO: Use a yaml configuration file for argument parsing instead of argparse
# FIXME: Replace >/dev/null occurrences for >>{logger_init.log_filename}
# FIXME: Directories are hardcoded into $HOME. obs-builder only works if placed in $HOME/obs-builder.py.
# FIXME: iterdir is more efficient than glob, replace all glob() ocurrences with iterdir.

import argparse
import subprocess
import re
import codecs
import fcntl
from subprocess import CalledProcessError
from pathlib import Path
from glob import glob
from os import path, remove
from shutil import move
from sys import exit
from logger import Logger
from json import dumps
from xml.dom import minidom
from xml.dom.minidom import Node
from urllib.request import urlretrieve
from urllib.error import HTTPError
from time import sleep

def get_lock_filepath():
    return "/tmp/obsbuilder.lock"

def create_lock_file():
    try:
        lock_file_path = get_lock_filepath()
        lock_file = open(lock_file_path, 'w')
        fcntl.flock(lock_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
        return lock_file
    except IOError:
        return None

def parse_giturl(aparser_giturl):
    giturlregex = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    giturl = aparser_giturl
    if giturl is None or not re.match(giturlregex, giturl):
        logger.error(f"Git URL is empty or not correct!")
        logger.error(f"Example: https://gitlab.com/example/example.git")
        exit(1)
    return giturl

def get_package_name(giturl):
    package = giturl.split("/")[-1]
    if ".git" in package:
        package = package[0:-4]
    return package

def exists_local_deb_repository(user, config, dist):
    return Path(f"/mnt/obs-builder/{user}/distpackages/{config}-{dist}").exists()

def exists_simple_local_deb_repository(user, config, dist):
    return Path(f"/usr/lib/local-apt-repository-{user}-{config}-{dist}").exists()

def get_local_deb_repository_path(user, config, dist):
    return Path(f"/mnt/obs-builder/{user}/packages/{config}-{dist}").absolute()

def get_simple_local_deb_repository_path(user, config, dist):
    return Path(f"/usr/lib/local-apt-repository-{user}-{config}-{dist}").absolute()

def get_package_version(package):
    if Path(f"{package}/debian/changelog").exists():
        return get_deb_package_version(package)
    else:
        return get_makedeb_package_version(package)

def download_package_source_url(source, destination):
    source_downloaded = False
    try:
        urlretrieve(source, destination)
        source_downloaded = True
    except HTTPError as e:
        # More useful
        logger.warning(f"Failed. Retrying with wget...")
        logger.debug(f"{e.code}: {e.reason}")
        logger.debug(f"Headers: {e.headers}")

    if source_downloaded is False:
        # Retry with wget
        try:
            subprocess.check_output(f"wget {source} -O {destination}", shell=True)
        except Exception as e:
            logger.error(f"Error when downloading source {path.basename(source)} from {source}!")
            raise


def get_makedeb_sha256sums(package, data_read):
    regex_sha256sum = re.compile(r"sha256sums=[^\)]*\)")
    if data_read is None or data_read == "":
        with open(f"{package}/PKGBUILD", mode="r", encoding="utf-8") as f:
            data_read = f.read()
    return re.findall(regex_sha256sum, data_read)[0].replace("\n", "")

def get_new_makedeb_sha256sums(package, new_pkgver, new_pkgrel, data_read, logger):
    regex_source = re.compile(r"(?<=source=\()[^\)]*")

    # This method is normally used by packages that need to be updated
    # Let's truncate the suffix
    package_name = package.replace("-update", "")

    if data_read is None or data_read == "":
        with open(f"{package}/PKGBUILD", mode="r", encoding="utf-8") as f:
            data_read = f.read()

    # Sources
    logger.info("Downloading sources if any...")
    if "source" in str(data_read):
        sources = re.findall(regex_source, data_read)

        # Sanitize
        try:
            # Packages such as Postman needs this. Package comes with newlines
            # Packages such as Pycharm Community comes without splitted spaces
            # Solution: replace newlines for spaces and split
            sources = sources[0].strip().replace("\t", "").replace("\n", " ").split(" ")
        except Exception:
            pass

        sources_sha256sums = []

        for source in sources:
            parsed_source = source.strip()

            # Like geckodriver makedeb package, eval may not be parsed correctly
            try:
                parsed_source = eval(source)
            except SyntaxError:
                parsed_source = f"{parsed_source}"

            # Some sources may have double :: to rename downloaded files
            # Remove everything that precedes it
            #
            # Example: libversion-3.0.3.tar.gz::https://github.com/repology/libversion/archive/3.0.3.tar.gz
            # to https://github.com/repology/libversion/archive/3.0.3.tar.gz
            # FIXME: Do not remove it, but rename the matched downloaded source to this name
            parsed_source = re.sub(r"^([\S\s]*?)\:\:", "", parsed_source)

            # FIXME: Only supports changing ${pkgname} and ${pkgver} variables
            parsed_source = parsed_source.replace("{", "")
            parsed_source = parsed_source.replace("}", "")
            parsed_source = parsed_source.replace("$pkgname", package_name)
            parsed_source = parsed_source.replace("$pkgver", new_pkgver)
            parsed_source = parsed_source.replace("$pkgrel", new_pkgrel)

            parsed_source_basename = path.basename(parsed_source)

            # HACK: strip character quotes
            parsed_source = parsed_source.strip("\"").strip("'")
            parsed_source_basename = parsed_source_basename.strip("\"").strip("'")

            if parsed_source_basename is None or parsed_source_basename.strip() == "":
                continue # Avoid blank sources

            logger.info(f"Found source {parsed_source_basename}! Downloading from {parsed_source}...")

            # Try downloading source
            # Skip if already downloaded
            location_posixpath = Path(f"{package}/{parsed_source_basename}")
            location = location_posixpath.absolute()
            if location_posixpath.exists():
                logger.info("Source file already downloaded!")
            else:
                try:
                    download_package_source_url(parsed_source, location)
                except Exception as e:
                    logger.error(e)
                    return False

            # Get sha256sum (suppress filename)
            source_sha256sum = subprocess.check_output(f"sha256sum {location} | cut -d ' ' -f 1", shell=True).decode("utf-8").strip()
            source_sha256sum_formatted = f"'{source_sha256sum}'"
            logger.info(f"Got checksum: {source_sha256sum_formatted}")
            sources_sha256sums.append(source_sha256sum_formatted)

        sources_sha256sums_formatted = f"sha256sums=({' '.join(str(x) for x in sources_sha256sums)})"
        return sources_sha256sums_formatted
    else:
        # No source file
        return ""

def replace_makedeb_package_version(package, pkgver, pkgrel):
    regex = re.compile(r"^pkg(ver|rel).{1,}")
    # We need pkgver and pkgrel, and join them together
    # Makedeb could also help here by using makedeb --print-control
    old_pkgrel = None
    old_pkgver = None

    pkgver = str(pkgver)
    pkgrel = str(pkgrel)

    with open(f"{package}/PKGBUILD", mode="r", encoding="utf-8") as f:
        data = f.readlines()
        f.seek(0)
        data_nosplit = f.read()

    # We also need to override sha256sums
    logger.info("Getting old sha256sums...")
    old_sha256sums = get_makedeb_sha256sums(package, data_nosplit)
    logger.info(old_sha256sums)

    logger.info("Getting new sha256sums...")
    new_sha256sums = get_new_makedeb_sha256sums(package, pkgver, pkgrel, data_nosplit, logger)

    if new_sha256sums is None:
        # Some error occurred, not replaced successfully.
        return False

    # Check that sha256sums are not equal
    # Warning because pkgrel may not be equal
    if old_sha256sums == new_sha256sums:
        logger.warning("Sources are the same! Package has already the specified version.")

    logger.info(f"New checksums: {new_sha256sums}")

    # Regex does not pick formatted \n Python
    regex_pkgrel = re.compile(r"(?<=pkgrel=)\'?[0-9]{1,}\'?")
    regex_pkgver = re.compile(r"(?<=pkgver=).*")
    regex_sha256sum = re.compile(r"sha256sums=[^\)]*\)")
    new_data = re.sub(regex_pkgver, pkgver, data_nosplit, 1)
    new_data = re.sub(regex_pkgrel, pkgrel, new_data, 1)
    new_data = re.sub(regex_sha256sum, new_sha256sums, new_data)
    replaced_sha256sums = True

    if replaced_sha256sums is False:
        logger.error("Some error occurred while replacing sha256sums!")
        return False

    with open(f"{package}/PKGBUILD", mode="w", encoding="utf-8") as f:
        f.write(new_data)
    return True

def get_makedeb_package_version(package):
    with open(f"{package}/PKGBUILD", mode="r", encoding="utf-8") as f:
        # We need pkgver and pkgrel, and join them together
        # Makedeb could also help here by using makedeb --print-control
        regex = re.compile(r"^pkg(ver|rel).{1,}")
        pkgrel = None
        pkgver = None
        for line in f:
            if pkgrel is None or pkgver is None:
                # Remove pkgver, pkgrel, whitespaces, quotes and equal sign
                if re.match(regex, line) and "pkgver" in line:
                    pkgver = line.replace("pkgver", "").replace("\"", "").replace("\'", "").replace("=", "").strip()
                if re.match(regex, line) and "pkgrel" in line:
                    pkgrel = line.replace("pkgrel", "").replace("\"", "").replace("\'", "").replace("=", "").strip()
            else:
                break
    return f"{pkgver}-{pkgrel}"

def override_deb_package_changelog(package, logger):
    old_version = subprocess.check_output(f"dpkg-parsechangelog --file {package}/debian/changelog --show-field Version", shell=True).decode("utf-8").rstrip()
    old_dist = subprocess.check_output(f"dpkg-parsechangelog --file {package}/debian/changelog --show-field Distribution", shell=True).decode("utf-8").rstrip()
    new_version = subprocess.check_output(f"dpkg-parsechangelog --file {package}/debian/changelog --show-field Version | cut -d '~' -f 1", shell=True).decode("utf-8").rstrip()
    new_dist = "unstable"

    # Only read the first line of the changelog
    with open(f'{package}/debian/changelog', 'r') as file:
        first_line = file.readline()

    first_line_parsed = first_line.replace(old_version, new_version).replace(old_dist, new_dist)

    # Open the file in read-write mode
    with open(f'{package}/debian/changelog', 'r+') as file:
        # Read the lines of the file into a list
        lines = file.readlines()

        # Replace the target line with a new line
        lines[0] = first_line_parsed

        # Move the file pointer to the beginning of the file
        file.seek(0)

        # Write the updated content back to the file
        file.writelines(lines)

        # Time to sync changes
        sleep(5)

        # Verify that the file was updated (sometimes needed on Chromium, it's a large changelog)
        #while True:
        #    with open(f'{package}/debian/changelog', 'r') as file:
        #        first_line = file.readline()
        #        if first_line == first_line_parsed:
        #            logger.info("Changelog successfully updated.")
        #            break
        #        else:
        #            logger.warning("Changelog update is still in progress. Waiting...")
        #            sleep(2)  # wait 2 seconds before trying again

def get_deb_package_version(package):
    # Old
    #return subprocess.check_output(f"dpkg-parsechangelog --show-field Version --file {package}/debian/changelog | rev | cut -d'-' -f2- | rev | sed 's/^[0-9]*://' | cut -d'.' -f1-3", shell=True).decode("utf-8").rstrip()

    # Examples:
    # (srb2) 2.2.10+git20221116.dc02339-1 should return 2.2.10+git20221116.dc02339
    # (schism) 2:20221201-2 should return 20221201

    return subprocess.check_output(f"dpkg-parsechangelog --show-field Version --file {package}/debian/changelog | rev | cut -d'-' -f2- | rev | sed 's/^[0-9]*://' | cut -d'+' -f1-2", shell=True).decode("utf-8").rstrip()
    #return subprocess.check_output(f"dpkg-parsechangelog --show-field Version --file {package}/debian/changelog | sed 's/-[^-]*$//' | sed 's/^[0-9]*://' | sed 's/\\+[^+]*$//'", shell=True).decode("utf-8").rstrip()

def get_deb_tar_package_version(package):
    # Old (does not remove XXX: prefix from changelog) (e.g.: schism)
    #return subprocess.check_output(f"dpkg-parsechangelog --show-field Version --file {package}/debian/changelog", shell=True).decode("utf-8").rstrip()

    return subprocess.check_output(f"dpkg-parsechangelog --show-field Version --file {package}/debian/changelog | sed 's/^[0-9]*://' ", shell=True).decode("utf-8").rstrip()

def has_deb_quilt_format(package):
    with open(Path(f'{package}/debian/source/format').absolute(), mode="r", encoding="utf-8") as f:
        if "quilt" in f.read():
            return True

def get_config_distro(config):
    return re.sub(r"\d+", '', config)

def create_log_file(pathfile):
    if Path(pathfile).exists():
        return False
    else:
        # Create parent directories
        subprocess.check_output(f"sudo mkdir -p {pathfile.replace(path.basename(pathfile), '')}", shell=True)

        # Create file
        subprocess.check_output(f"sudo touch {pathfile}", shell=True)
        return True

def create_sources_list(repos):
    # Create sources.list that we will use for Makedeb for updating repositories
    with open("sources-chroot.list", mode="w", encoding="utf-8") as f:
        for repo in repos:
            # Debian and Ubuntu follows the scheme $URL/debian/codename/category
            # So, transform codename and category backslashes into spaces
            # e.g.: 'http://deb.debian.org/debian/bullseye/main' -> 'http://deb.debian.org/debian bullseye main'
            parts = repo.rsplit('/', 2)
            transformed_url = ' '.join(parts[:-1]) + ' ' + parts[-1]
            f.write(f"deb [trusted=yes] {transformed_url}\n")

def install_from_backports(package):
   # This may fail if the package does not exist in backports
   try:
       subprocess.check_call(f"sudo apt-get -t bullseye-backports -yq install {package}", shell=True, stdout=subprocess.DEVNULL)
   except subprocess.CalledProcessError:
       logger.warning("{package} not installed from backports")

def check_host_requirements(logger): # install pahole due to newer kernel versions (v6.6 LTS and up)
    subprocess.check_call(f"sudo apt-get -yq install qemu-user-static reprepro zstd binfmt-support git pahole obs-build liblwp-useragent-determined-perl rng-tools gnupg2 dpkg-dev python3-prettytable devscripts wget && sudo service binfmt-support restart  >/dev/null", shell=True, stdout=subprocess.DEVNULL)
    install_from_backports("obs-build")
    install_from_backports("binfmt-support")
    install_from_backports("qemu-user-static")
    install_from_backports("reprepro")
    install_from_backports("pahole")

def query_arch_all(package, orig_arch):
    if Path(f"{package}/debian/control").exists():
        with open(f"{package}/debian/control", mode="r", encoding="utf-8") as f:
            arch_all = False
            for line in f:
                if re.match(r"^Architecture:\sall", line):
                    arch_all = True
                    break
    else:
        # Makedeb package
        arch_all = False

    if arch_all:
        return "all"
    return orig_arch

def get_package_output_dir(user, config, dist):
    return f"/mnt/obs-builder/{user}/distpackages/{config}-{dist}"

def get_log_package(user, config, dist, package, version, suffix, arch):
    if suffix != "":
        suffix = f"-{suffix}"
    package_output_dir = get_package_output_dir(user, config, dist)
    arch = query_arch_all(package, arch)
    log_package = f"{package_output_dir}/{package}-{version}{suffix}-{arch}.log"
    return log_package

def check_already_built_package(user, config, dist, package, version, suffix, arch):
    log_package = get_log_package(user, config, dist, package, version, suffix, arch)
    try:
        check_success_log = Path(log_package).read_text(encoding="utf-8").rstrip() == "success"
    except FileNotFoundError:
        check_success_log = False
    return check_success_log

def query_built_status(built_package):
    if built_package is True:
        return "BUILT"
    elif built_package == "SKIP":
        return "SKIP"
    elif built_package is False:
        return "FAILED"

def cleanall_build_environment(preserve_file_list):
    clean_build_environment()
    subprocess.check_call(f"sudo rm -rf /mnt/obs-builder", shell=True)
    subprocess.check_call(f"/usr/lib/local-apt-repository-*", shell=True)
    subprocess.check_call(f"/srv/local-apt-repository-*", shell=True)
    subprocess.check_call(f"rm -rf tmp/*", shell=True)
    subprocess.check_call(f"rm -rf log/*", shell=True)
    subprocess.check_call(f"rm -rf __pycache__", shell=True)
    subprocess.check_call(f"rm -rf tmp/*", shell=True)

def clean_build_environment():
    subprocess.check_output(f"sudo obs-build --wipe >/dev/null 2>&1", shell=True)
    subprocess.check_output(f"sudo rm -rf -- /var/tmp/obs-build-root/* >/dev/null 2>&1", shell=True)
    subprocess.check_output(f"sudo rm -rf -- /var/cache/obs-build >/dev/null 2>&1", shell=True)

    # FIXME: Point obs-build cachedir to obs-build instead of build, see source
    subprocess.check_output(f"sudo rm -rf -- /var/cache/build >/dev/null 2>&1", shell=True)

    # Remove deb packages from /
    subprocess.check_output(f"sudo rm -rf /*.deb", shell=True)

    # Remove all files from repository except the ones from allowlist
    # TODO: Set correct path to avoid disaster
    """
    command = "find . -mindepth 1 -maxdepth 2"
    for filelist in preserve_file_list:
        command = f"{command} -not -path {filelist}"
    remove_all = r"-exec rm -rf {} \;"
    command = f"{command} -exec {remove_all}"
    """

def clone_package(package, giturl, suffix):
    # Clone repository
    if not Path(package).exists() or not Path(f"{package}/.git").exists():
        logger.info(f"Cloning repository from package {package}")
        subprocess.check_call(f"rm -rf {package} && git clone --quiet --depth=1 --single-branch --no-tags {giturl}", shell=True)
    else:
        logger.info(f"Skipping repository cloning, '{package}' directory already exists.")
        logger.info(f"Performing Git pull...")
        subprocess.check_call(f"git -C {package} pull -q --ff", shell=True)

def build_deb_package(user, config, dist, package, suffix, arch, repos, logger, force_build, skip_git_check):
    # TODO
    #if additional_repositories is None:
    #    additional_repositories = ""

    # TODO
    # Build for host architecture is package is for ALL architectures

    # Building with obs-build requires $PKGNAME_$PKGVER.tar.xz and $PKGNAME_$PKGVER.dsc
    # `dpkg-source -b .` command requires $PKGNAME_$PKGVER.orig.tar.xz (assuming Git repository already contains debian/ subdirectory)

    # Override ~deb11u1 suffix from Debian changelog if exists (if package is picked from stable or security repo instead)
    override_deb_package_changelog(package, logger)

    # Get version of the package: https://askubuntu.com/a/423534
    # Remove -YYY revision in package if exists
    #package_version_debian = get_deb_package_version(package)
    #package_version = re.sub(r"-[0-9]{1,}$", "", package_version_debian)

    # Remove "YY:" prefix number from package if exists (example: meta-gnome3)
    #package_version = re.sub(r"^[0-9]{1,}\:", "", package_version)
    # Workaround: in package_version_debian is also needed
    #package_version_debian = re.sub(r"^[0-9]{1,}\:", "", package_version_debian)

    package_version = get_deb_package_version(package)
    package_version_debian = get_deb_tar_package_version(package)

    logger.info(f"Parsing version from package '{package}'... Got '{package_version}'")

    # Check that package is already built or not (or force_build is set to True) and if it has succeeded
    if check_already_built_package(user, config, dist, package, package_version_debian, suffix, arch) and not force_build:
        logger.info(f"Skipping already built {package} (version {package_version_debian}) for arch {arch}. Note: use --force if you want to build it anyway.")
        return "SKIP"
    else:
        # Clean previous leftovers
        clean_build_environment()

        # Generate .orig.tar.xz file from Git source.
        # Move .git folder out
        if not skip_git_check:
            logger.debug(f"Moving git folder out of the way...")
            subprocess.check_output(f"rm -rf .{package}-gitinfo && mv {package}/.git .{package}-gitinfo", shell=True)

        # Change folder package name to identifiable source
        logger.debug("Renaming src...")
        subprocess.check_output(f"rm -rf {package}-{package_version_debian} && mv {package} {package}-{package_version_debian}", shell=True)

        # Compress with xz
        logger.info("Compressing package...")
        subprocess.check_output(f"XZ_OPT=-0 tar -cJf {package}_{package_version}.orig.tar.xz {package}-{package_version_debian} --force-local", shell=True)

        # Generate .dsc file and .debian.tar.xz from source
        logger.info("Generating .dsc file...")
        subprocess.check_output(f"dpkg-source --include-binaries -b {package}-{package_version_debian}", shell=True)

        # Rename .debian.tar.xz
        # We need this depending on debian format. .debian.tar.xz file is needed for quilt format.
        # .tar.xz is needed for native format
        debformat = "quilt" if "quilt" in open(f"{package}-{package_version_debian}/debian/source/format", mode="r", encoding="utf-8").read() else "native"

        if debformat == "quilt":
            logger.info("Generating debian .tar.xz file...")
            if Path(f"{package}_{package_version_debian}.tar.xz").exists():
                subprocess.check_output(f"rm -rf {package}_{package_version_debian}.debian.tar.xz && mv {package}_{package_version_debian}.tar.xz {package}_{package_version_debian}.debian.tar.xz", shell=True)

        # Rename directory as it was before
        subprocess.check_output(f"rm -rf {package} && mv {package}-{package_version_debian} {package}", shell=True)

        # Restore git directory
        if not skip_git_check:
            subprocess.check_output(f"rm -rf {package}/.git && mv .{package}-gitinfo {package}/.git", shell=True)

        # Prepare OBS build
        obs_build_dir = f"{package}-obs"

        suffix = ""
        if debformat == "quilt":
            # with .debian.tar.xz file
            suffix = ".debian"
        subprocess.check_output(fr"rm -rf {obs_build_dir} && mkdir -p {obs_build_dir} && mv {package}_{package_version}.orig.tar.xz {obs_build_dir} && mv {package}_{package_version_debian}{suffix}.tar.xz {obs_build_dir} && mv {package}_{package_version_debian}.dsc {obs_build_dir}", shell=True)

        # Clean leftovers
        clean_build_environment()

        # 16h timeout
        log_package = get_log_package(user, config, dist, package, package_version_debian, suffix, arch)
        if Path(log_package).exists():
            logger.warning(f"Log {log_package} already exists.")
        try:
            logger.info(f"Deleting original package {package}...")
            exec_remove = r"rm -rf {} \;"
            subprocess.check_output(f"find {package} -mindepth 1 -maxdepth 1 -type d ! -name 'debian' -exec {exec_remove}", shell=True)
            subprocess.check_output(f"find {package} -mindepth 1 -maxdepth 1 -type f -exec {exec_remove}", shell=True)

            # Copy resolv.conf into rootfs
            logger.info(f"Enabling network in chroot, before building...")
            subprocess.check_output(f"sudo rm -rf /var/tmp/obs-build-root/etc/resolv.conf && sudo mkdir -p /var/tmp/obs-build-root/etc/ && sudo cp /etc/resolv.conf /var/tmp/obs-build-root/etc/resolv.conf", shell=True)

            logger.info(f"Building package {package} (version {package_version_debian}) for arch {arch}...")
            # Uncomment this if no custom repo is still set
            logger.debug(f"Running command: sudo obs-build {repos} --arch {arch} --dist {config} --cachedir=/var/cache/obs-build {obs_build_dir} >>{logger_init.log_filename} 2>&1")
            subprocess.check_output(f"sudo obs-build {repos} --arch {arch} --dist {config} --cachedir=/var/cache/obs-build {obs_build_dir} >>{logger_init.log_filename} 2>&1", shell=True)
            logger.info(f"Built package {package} (version {package_version_debian}).")
            return True
        except Exception:
            logger.error(f"Failed building package {package} (version {package_version_debian}) for arch {arch}!")
            if Path("log/log.txt").exists() and Path("/var/tmp/obs-build-root/home/abuild/log.txt").exists():
                with open("log/log.txt", mode="a+", encoding="utf-8") as f:
                    f.write(open(f"/var/tmp/obs-build-root/home/abuild/log.txt").read())
            return False

def build_makedeb_package(user, config, dist, package, suffix, arch, repos, logger, force_build):
    # MAKEDEB
    # Workaround: we will use a dummy package to invoke a shell inside generated chroot, copy needed files and build the package
    # -X python3: install extra package
    # Makedeb requires ca-certificates, sudo, wget, apt, dpkg, user with sudo, resolv.conf, sources.list

    # Dependencies
    # sudo apt-get download makedeb sudo curl apt python3-apt zstd libarchive-tools libapt-pkg6.0 libapt-pkg6.0 distro-info-data lsb-release libarchive13 libcurl4 gpgv2 python-apt-common libnghttp2-14 librtmp1 librtmp1 libldap-2.4-2 libbrotli1 libxxhash0 gpgv debian-archive-keyring libssh2-1 libsasl2-2 ca-certificates openssl libsasl2-modules-db
    # Add abuild user to sudo: adduser abuild sudo
    # etc/apt/sources.list.d/official-repos.list (useful)
    # apt-rdepends, libapt-pkg-perl -> For caching dependencies instead of downloading them for each release

    makedeb_version = get_makedeb_package_version(package)
    if check_already_built_package(user, config, dist, package, makedeb_version, suffix, arch) and not force_build:
        logger.info(f"Skipping already built {package} (version {makedeb_version}) for arch {arch}. Note: use --force if you want to build it anyway.")
        return "SKIP"
    else:
        # Clean first
        clean_build_environment()

        # Copy script file and set permissions
        logger.info("Copying makedeb script...")
        subprocess.check_output(f"sudo mkdir -p /var/tmp/obs-build-root/home/abuild/ && sudo rm -rf /var/tmp/obs-build-root/home/abuild/makedeb-builder.py && sudo cp -r makedeb-builder.py /var/tmp/obs-build-root/home/abuild/ && sudo chmod -R 777 /var/tmp/obs-build-root/home/abuild/makedeb-builder.py", shell=True)

        # Copy makedeb package
        logger.info("Copying makedeb package...")
        subprocess.check_output(f"sudo cp -r ~/external/makedeb/makedeb_16.0.0-stable_all.deb /var/tmp/obs-build-root/home/abuild/ && sudo chmod -R 777 /var/tmp/obs-build-root/home/abuild/makedeb_16.0.0-stable_all.deb", shell=True)

        # Copy git file and set permissions
        logger.info(f"Preparing environment for package {package} (version {makedeb_version}) and arch {arch}...")
        subprocess.check_output(f"sudo mkdir -p /var/tmp/obs-build-root/home/abuild/ && sudo rm -rf /var/tmp/obs-build-root/home/abuild/{package} && sudo cp -r {package} /var/tmp/obs-build-root/home/abuild", shell=True)

        # Create destination repository and set permissions
        subprocess.check_output(f"sudo rm -rf /var/tmp/obs-build-root/usr/src/packages/DEBS && sudo mkdir -p /var/tmp/obs-build-root/usr/src/packages/DEBS && sudo chmod -R 777 /var/tmp/obs-build-root/usr/src/packages/DEBS", shell=True)

        # Set new log
        subprocess.check_output(f"sudo rm -rf /var/tmp/obs-build-root/home/abuild/log.txt && sudo touch /var/tmp/obs-build-root/home/abuild/log.txt && sudo chmod 777 /var/tmp/obs-build-root/home/abuild/log.txt", shell=True)
        logger_init.set_log_filename(f"/var/tmp/obs-build-root/home/abuild/log.txt")

        # Copy /etc/mtab (required for makedeb because it uses /dev/fd inodes)
        subprocess.check_output(f"sudo mkdir -p /var/tmp/obs-build-root/etc/ && sudo cp /etc/mtab /var/tmp/obs-build-root/etc/mtab", shell=True)

        # Copy resolv.conf into rootfs
        subprocess.check_output(f"sudo rm -rf /var/tmp/obs-build-root/etc/resolv.conf && sudo mkdir -p /var/tmp/obs-build-root/etc/ && sudo cp /etc/resolv.conf /var/tmp/obs-build-root/etc/resolv.conf", shell=True)

        # Generate and copy sources.list into rootfs
        url_repos = [elem for elem in repos.split(" --repository ") if elem]
        create_sources_list(url_repos)
        subprocess.check_output(f"sudo rm -rf /var/tmp/obs-build-root/etc/apt/sources.list && sudo mkdir -p /var/tmp/obs-build-root/etc/apt/ && sudo cp sources-chroot.list /var/tmp/obs-build-root/etc/apt/sources.list", shell=True)

        # Copy abuild file into /etc/sudoers.d
        subprocess.check_output(f"sudo rm -rf /var/tmp/obs-build-root/etc/sudoers.d/abuild && sudo mkdir -p /var/tmp/obs-build-root/etc/sudoers.d/ && sudo cp abuild /var/tmp/obs-build-root/etc/sudoers.d/abuild && sudo chown root:root /var/tmp/obs-build-root/etc/sudoers.d/abuild", shell=True)

        extra_packages = "-X wget -X build-essential -X ca-certificates -X apt -X apt-utils -X sudo -X passwd"

        shell_cmd = f"/usr/bin/python3 makedeb-builder.py --name '{package}' >>{logger_init.log_filename} 2>&1"

        # Prebuilt (before mount points enabled)
        # Note: This will always fail
        subprocess.run(f"sudo obs-build -X python3 {extra_packages} {repos} --arch {arch} --dist {config} --cachedir=/var/cache/obs-build test-makedeb-obs-sample >>{logger_init.log_filename} 2>&1", shell=True)
        try:
            # Set mount points
            #subprocess.check_output(f"sudo mkdir -p /var/tmp/obs-build-root/proc && sudo mount -t proc /proc /var/tmp/obs-build-root/proc", shell=True)
            #subprocess.check_output(f"sudo mkdir -p /var/tmp/obs-build-root/sys && sudo mount -t sysfs /sys /var/tmp/obs-build-root/sys", shell=True)
            #subprocess.check_output(f"sudo mkdir -p /var/tmp/obs-build-root/dev && sudo mount -o bind /dev /var/tmp/obs-build-root/dev", shell=True)
            #subprocess.check_output(f"sudo mkdir -p /var/tmp/obs-build-root/dev/fd && sudo mount -o bind /dev/fd /var/tmp/obs-build-root/dev/fd", shell=True)
            #subprocess.check_output(f"sudo mkdir -p /var/tmp/obs-build-root/run && sudo mount -o bind /run /var/tmp/obs-build-root/run", shell=True)

            #mount -t proc /proc proc/
            #mount -t sysfs /sys sys/
            #mount -o bind /dev dev/
            subcommand = r"runuser -l abuild -c '/usr/bin/python3 ~/makedeb-builder.py'"

            # 16h timeout
            logger.info(f"Building package {package} (version {makedeb_version}) for arch {arch}...")
            subprocess.check_output(f"sudo arch-chroot /var/tmp/obs-build-root /bin/bash -c \"runuser -l abuild -c '/usr/bin/python3 ~/makedeb-builder.py --name {package}'\"", shell=True)
            logger.info(f"Package {package} (version {get_makedeb_package_version(package)}) built successfully!")
            logger.info(f"Deleting original package {package}...")
            exec_remove = r"rm -rf {} \;"
            subprocess.check_output(f"find {package} -mindepth 1 -maxdepth 1 -type f ! -name 'PKGBUILD' -exec {exec_remove}", shell=True)
            return True
        except Exception:
            logger.error(f"Failed building package {package} (version {makedeb_version}) for arch {arch}!")
            if Path("log/log.txt").exists() and Path("/var/tmp/obs-build-root/home/abuild/log.txt").exists():
                with open("log/log.txt", mode="a+", encoding="utf-8") as f:
                    f.write(open(f"/var/tmp/obs-build-root/home/abuild/log.txt").read())
            return False

def build_package(package, user, arch, giturl, dist, suffix, config, repos, logger, force_build, skip_git_check, type_package="Deb"):
    logger.info(f"Retrieving package {package} for arch {arch}...")
    if not skip_git_check:
        clone_package(package, giturl, "")

    # DEB PACKAGE
    if type_package == "Deb":
        return build_deb_package(user, config, dist, package, suffix, arch, repos, logger, force_build, skip_git_check)
    else:
        return build_makedeb_package(user, config, dist, package, suffix, arch, repos, logger, force_build)

def rebuild_index_simple_local_deb_repository(deb_location, local_debian_repository):
    subprocess.check_output(f"sudo bash local-apt-repository/rebuild.sh -f {deb_location} {local_debian_repository}", shell=True)

def generate_simple_local_deb_repository(arch, user, config, dist, nodbgsym, suffix, logger):
    # Write generated debs to array
    obs_build_debs = "/var/tmp/obs-build-root/usr/src/packages"
    generated_debian_packages = []
    for deb in glob(f"{obs_build_debs}/*.deb"):
        generated_debian_packages.append(path.basename(deb))

    for deb in glob(f"{obs_build_debs}/DEBS/*.deb"):
        generated_debian_packages.append(path.basename(deb))

    if nodbgsym:
        logger.info("Not copying package debug symbols...")
        for deb in generated_debian_packages:
            if "dbgsym" in deb:
                generated_debian_packages.remove(path.basename(deb))
    else:
        logger.info("Copying package debug symbols...")

    # Set local Debian repository based on user, config and dist
    folder_local_deb_repository = f"local-apt-repository-{user}-{config}-{dist}"

    if suffix != "":
        folder_local_deb_repository = f"local-apt-repository-{user}-{config}-{dist}-{suffix}"

    local_debian_repository = f"/usr/lib/{folder_local_deb_repository}"
    logger.info(f"Setting local repository to {local_debian_repository}...")
    subprocess.check_output(f"sudo mkdir -p {local_debian_repository}", shell=True)

    # Set deb package placement based on user, config and dist
    deb_location = f"/srv/{folder_local_deb_repository}"
    logger.info(f"Packages location will be in {deb_location}")
    subprocess.check_output(f"sudo mkdir -p {deb_location}", shell=True)

    logger.info(f"Copying files...")
    for debpackage in generated_debian_packages:
        if(Path(f"{deb_location}/{debpackage}").exists()):
            subprocess.check_output(f"sudo rm -rf {deb_location}/{debpackage}", shell=True)
            rebuild_index_simple_local_deb_repository(deb_location, local_debian_repository)
        # One of the two locations will always miss
        subprocess.run(f"sudo cp -f {obs_build_debs}/{debpackage} {deb_location}; sudo cp -f {obs_build_debs}/DEBS/{debpackage} {deb_location}", shell=True, stderr=subprocess.DEVNULL)

    # Check that package is already built or not (or force_build is set to True)
    package_version = get_package_version(package)

    # Workaround: Remove "YY:" prefix number from package if exists (example: meta-gnome3)
    package_version = re.sub(r"^[0-9]{1,}\:", "", package_version)

    package_output_dir = get_package_output_dir(user, config, dist)
    log_package = get_log_package(user, config, dist, package, package_version, suffix, arch)

    # In theory log is already created, let's put some words
    logger.info("Reporting success status...")
    subprocess.check_output(f"sudo mkdir -p {package_output_dir} && sudo rm -rf {log_package} && echo 'success' | sudo tee -a {log_package} > /dev/null", shell=True)

    logger.info(f"Populating local repository index...")
    rebuild_index_simple_local_deb_repository(deb_location, local_debian_repository)

    logger.info(f"Fixing permissions...")
    subprocess.check_output(f"sudo chown -R {user}:{user} {deb_location} && sudo chown -R {user}:{user} {local_debian_repository}", shell=True)

def check_gnupg_service(user, gpg_key_name):
    # Configure GNUPG
    gnupg_home = f"/home/{user}/.gnupg"
    subprocess.check_output(f"mkdir -p {gnupg_home} && cp gnupg/gpg-agent.conf {gnupg_home}/gpg-agent.conf && cp gnupg/gpg.conf {gnupg_home}/gpg.conf", shell=True)
    file_permissions = r"-type f -exec chmod 600 {}"
    dir_permissions = r"-type d -exec chmod 700 {}"
    subprocess.check_output(f"sudo chown -R {user}:{user} {gnupg_home} && find {gnupg_home} {file_permissions} \; && find {gnupg_home} {dir_permissions} \;", shell=True)
    subprocess.check_output(f"echo RELOADAGENT | gpg-connect-agent >/dev/null", shell=True)
    logger.info("Gnupg service started successfully.")

def sign_deb_packages(arch, user, config, dist, signkey, suffix, logger):
    folder_local_deb_repository = f"local-apt-repository-{user}-{config}-{dist}"

    if suffix != "":
        folder_local_deb_repository = f"local-apt-repository-{user}-{config}-{dist}-{suffix}"

    deb_location = f"/srv/{folder_local_deb_repository}"

    # Create signed repository
    Path(f"{deb_location}/signed").mkdir(exist_ok=True)

    # Create archive repository
    Path(f"{deb_location}/archive").mkdir(exist_ok=True)

    # SignWith -> get gpg key id
    # https://stackoverflow.com/a/66242583
    logger.info("Checking gnupg service...")

    # FIXME: Import provided secret key in arguments
    check_gnupg_service(user, "Custom Debian Repository Signing Key")
    gpg_key_sig_id = subprocess.check_output(f"gpg2 --list-signatures --with-colons | grep 'sig' | grep 'Custom Debian Repository Signing Key' | head -n 1 | cut -d':' -f5", shell=True).decode("utf-8").rstrip()

    signed_directory = Path(f"{deb_location}/signed").absolute()
    logger.info("Signing deb packages...")
    for deb in glob(f"{deb_location}/*.deb"):
        subprocess.check_output(f"dpkg-sig -k {gpg_key_sig_id} --sign builder {deb} --gpg-options '--passphrase-file keys/password.txt'", shell=True)
        move(f"{deb}", f"{signed_directory}/{path.basename(deb)}")
        logger.info(f"Signed {path.basename(deb)}")

def generate_conf_distributions(deb_repository_folder, dist, origin, label, logger):
    logger.info("Generating conf distributions file...")
    # SignWith -> get gpg key id
    # https://stackoverflow.com/a/66242583
    gpg_key_sig_id = subprocess.check_output(f"gpg2 --list-signatures --with-colons | grep 'sig' | grep 'Custom Debian Repository Signing Key' | head -n 1 | cut -d':' -f5", shell=True).decode("utf-8").rstrip()

    # Generate distributions file
    # Architectures: Debian supported architectures
    debian_supported_architectures = "amd64 arm64 armel armhf i386 mips64el mipsel ppc64el s390x"
    distributions_content = f"Origin: {origin}\nLabel: {label}\nCodename: {dist}\nComponents: main\nArchitectures: {debian_supported_architectures}\nSignWith: {gpg_key_sig_id}\n"

    # FIXME: If the release is the same, do not write.
    # However, if more than one release has to be supported at the time, do not overwrite: append content.

    if not Path(f"{deb_repository_folder}/conf/distributions").exists():
        with open(f"tmp/distributions-{dist}-{gpg_key_sig_id}", mode="w", encoding="utf-8", newline='') as f:
            f.write(distributions_content)

        # Copy distributions file
        subprocess.check_output(f"sudo mkdir -p {deb_repository_folder}/conf && sudo cp tmp/distributions-{dist}-{gpg_key_sig_id} {deb_repository_folder}/conf/distributions", shell=True)
    else:
        logger.info(f"File is already generated in {deb_repository_folder}")

def setup_deb_repository(package, user, config, dist, repourl, origin, label, suffix, logger, force_build=False):
    if suffix != "":
        f_suffix = f"-{suffix}"
    else:
        f_suffix = ""

    folder_local_deb_repository = f"local-apt-repository-{user}-{config}-{dist}{f_suffix}"
    signed_directory = f"/srv/{folder_local_deb_repository}/signed"
    archive_directory = f"/srv/{folder_local_deb_repository}/archive"

    www_deb_repository_root = f"/var/www/aat-obsbuilder-repository{f_suffix}"

    # Create debian directory
    distro = get_config_distro(config)
    www_deb_repository = f"{www_deb_repository_root}/{distro}"
    subprocess.check_output(f"sudo mkdir -p '{www_deb_repository}'", shell=True)

    # Fix permissions in order to use reprepro with unprivileged user
    subprocess.check_output(f"sudo chown -R {user}:{user} '{www_deb_repository}'", shell=True)

    generate_conf_distributions(www_deb_repository, dist, origin, label, logger)

    logger.info("Deleting existing package from reprepro if exists...")
    for deb in glob(f"{signed_directory}/*.deb"):
        # FIXME: This command does not contemplate subpackages. E.g: mysql-workbench, mysql-workbench-data, lpairs2, lpairs2-data, lpairs2-doc...
        # Possible solution: for loop -> dpkg-deb --info lpairs2_2.2-1_amd64.deb | grep Package | cut -d ':' -f2 | xargs
        remove_command = r"rm -rf {}  \;"
        filter_command = r"awk '{print $1}'"
        deb_package = subprocess.check_output(f"dpkg-deb --show {deb} | {filter_command}", shell=True).decode("utf-8").rstrip()

        # Using reprepro 3.5.1 onwards
        deb_package_new = subprocess.check_output("reprepro -b {} list {} | grep '{}|' | awk -F'[| ]' '{{print $4\"|\"$5}}' | head -n1".format(www_deb_repository, dist, deb_package), shell=True).decode("utf-8").strip()

        logger.debug(f"Running command 'reprepro -b {www_deb_repository} remove {dist} \'{deb_package}\''...")
        subprocess.run(f"reprepro -b {www_deb_repository} remove {dist} '{deb_package}' >>{logger_init.log_filename}", shell=True)

        # Using reprepro 3.5.1 onwards
        logger.debug(f"Running command 'reprepro -b {www_deb_repository} remove {dist} \'{deb_package_new}\''...")
        subprocess.run(f"reprepro -b {www_deb_repository} remove {dist} '{deb_package_new}' >>{logger_init.log_filename}", shell=True)

        # Remove package
        subprocess.run(f"sudo find {www_deb_repository} -type d -name {path.basename(deb)} -exec {remove_command}", shell=True)

    # Clean reprepro (remove pool packages that do not exist on conf/distributions) (run regardless of errors)
    logger.info("Cleaning orphaned packages from the repository...")
    subprocess.run(f"reprepro -b {www_deb_repository} dumpunreferenced >>{logger_init.log_filename}", shell=True)
    subprocess.run(f"reprepro -b {www_deb_repository} deleteunreferenced >>{logger_init.log_filename}", shell=True)
    subprocess.run(f"reprepro -b {www_deb_repository} clearvanished >>{logger_init.log_filename}", shell=True)
    subprocess.run(f"reprepro -b {www_deb_repository} export >>{logger_init.log_filename}", shell=True)

    for deb in glob(f"{signed_directory}/*.deb"):
        try:
            logger.info(f"Including {package} in repository...")
            logger.debug(f"Running command: reprepro -b {www_deb_repository} includedeb {dist} {deb}")
            subprocess.check_output(f"reprepro -b {www_deb_repository} includedeb {dist} {deb} >>{logger_init.log_filename} 2>&1", shell=True)

        # This fails if it is a makedeb package. Set it manually
        except CalledProcessError:
            logger.warning(f"Inclusion of {package} failed. Retrying with hardcoded values...")
            subprocess.check_output(f"reprepro -b {www_deb_repository} -S utils -C main -P optional includedeb {dist} {deb} >/dev/null", shell=True)

        logger.info(f"Package '{package}' included successfully for distribution {dist} in {www_deb_repository}!")
        logger.info(f"Moving {deb} to archive...")
        move(f"{deb}", f"{archive_directory}/{path.basename(deb)}")

    # Copy key
    subprocess.check_output(f"sudo cp 'keys/debports.rsa.pub.gpg' '{www_deb_repository_root}/aat-debian-repository.rsa.pub.key'", shell=True)

    # Set ownership as root
    subprocess.check_output(f"sudo chown -R root:root '{www_deb_repository}'", shell=True)

def update_package_debian(debemail, debfullname, update_build_dir, package, newversion, user, arch, giturl, dist, suffix, config, repos, logger, force, type_package, upload_repository):
    # Scan debian/watch for new uploads and parse output

    # This step may fail with: "scan die: FAIL Checking OpenPGP signature (no signature file downloaded)."
    # Expected output:
    # uscan: Newest version of hello on remote site is 2.12.1, local version is 2.10
    # uscan:  => Newer package available from:
    #         => https://ftp.gnu.org/gnu/hello/hello-2.12.1.tar.gz
    # uscan die: FAIL Checking OpenPGP signature (no signature file downloaded).
    url_regex = re.compile(r"http[s]?:\/\/(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")
    try:
        upstream_package = subprocess.check_output(f"uscan {package}-update", shell=True, stderr=subprocess.STDOUT).decode("utf-8").strip().replace("\n", " ")
    except CalledProcessError as e:
        upstream_package = (e.output).decode("utf-8").strip().replace("\n", " ")
    parsed_upstream_package = url_regex.search(upstream_package).group(0).split("/")[-1]

    update_output = subprocess.check_output(fr"uscan --dehs {package}-update --skip-signature", shell=True).decode("utf-8").strip()
    parsed_update_output = minidom.parseString(update_output)

    new_upstream_warnings = None
    new_upstream_errors = None
    new_upstream_status = None
    new_upstream_version = None
    new_upstream_url = None
    logger.debug("Printing updated package information...")
    for elem in parsed_update_output.getElementsByTagName('dehs'):
        for x in elem.childNodes:
            # We only want upstream version and upstream-url instead of all nodes
            if x.nodeType == Node.ELEMENT_NODE:
                if x.tagName == "errors":
                    new_upstream_errors = x.childNodes[0].data
                if x.tagName == "warnings":
                    new_upstream_warnings = x.childNodes[0].data
                if x.tagName == "status":
                    new_upstream_status = x.childNodes[0].data
                if x.tagName == "upstream-version":
                    new_upstream_version = x.childNodes[0].data
                if x.tagName == "upstream-url":
                    new_upstream_url = x.childNodes[0].data
                logger.debug(f"{x.tagName}: {x.childNodes[0].data}")
    # Check for errors / skip update
    if new_upstream_status == "only older package available":
        logger.info(f"Skipping update for package {package}, only older package available!")
    elif new_upstream_errors is not None:
        logger.error(f"Error when updating package {package}: {new_upstream_errors}")
    elif new_upstream_warnings is not None:
        logger.error(f"Warning when updating package {package}: {new_upstream_warnings}")
    elif new_upstream_version is None:
        logger.error(f"No upstream status information when updating package {package}!")
    elif new_upstream_status == "newer package available":
        # Update package
        logger.info(f"Preparing update for package {package}...")
        logger.info(f"New upstream version {new_upstream_version} from {new_upstream_url}")

        package_newversion = f'{package}-{new_upstream_version}'

        # Update to newer version
        # TODO: Check if we need to put a new revision instead of a newer version (We will use -b to force writing version)
        revision = 1
        if revision:
            message = "New revision released."
        else:
            message = "New upstream release."

        # Distribution: hardcode to unstable
        distribution = "unstable"

        # FIXME: Avoid new upstream checking if this variable is provided
        if newversion is not None:
            new_upstream_version = newversion

        logger.info("Copying debian folder from old location and adapt it to the newer version...")
        subprocess.check_output(f"export DEBEMAIL='{debemail}'; export DEBFULLNAME='{debfullname}'; cd '{update_build_dir}' && uupdate -b -v '{new_upstream_version}' '../{parsed_upstream_package}'; cd ..", shell=True)

        logger.info("Writing new upstream version / revision to changelog...")
        subprocess.check_output(f"export DEBEMAIL='{debemail}'; export DEBFULLNAME='{debfullname}'; cd '{package_newversion}' && dch --distribution {distribution} -b -v '{new_upstream_version}+dfsg-{revision}' {message}; cd ..", shell=True)

        package_newversion = f'{package}-{new_upstream_version}+dfsg'
        logger.info(f"Deleting old package directory...")
        subprocess.check_output(f"rm -rf '{package}' && mv '{package_newversion}' '{package}' > /dev/null", shell=True)
        subprocess.check_output(f"cp -r '{package}-update/.git' '{package}' > /dev/null", shell=True)

        logger.info(f"Rebuilding package {package}, version {new_upstream_version} for arch {arch}...")

        # Build package to verify a successful update
        return build_deb_package(user, config, dist, package, suffix, arch, repos, logger, force, False)
    else:
        # Unknown error!
        logger.error(f"Unknown status when updating package {package}: {new_upstream_status}")
        return False

def update_package_makedeb(update_build_dir, package, newversion, giturl, dist, suffix, config, repos, logger, force, type_package, upload_repository):
    # Verify that PKGBUILD exists
    pkgbuild_file = f"{update_build_dir}/PKGBUILD"
    if not Path(pkgbuild_file).exists():
        logger.error(f"PKGBUILD does not exist in {package}!")
        return False

    # Parse pkgver version (if 3.10 is provided, 3.10 is pkgver, 0 is pkgrel)
    new_pkgver = newversion.split("-")[0]
    try:
        new_pkgrel = newversion.split("-")[1]
    except IndexError:
        new_pkgrel = 0

    logger.info(f"New version provided for package {package}. pkgver: {new_pkgver}, pkgrel: {new_pkgrel}.")

    # Parse pkgrel version (if 3.10-1 is provided, 1 is pkgrel version)
    logger.info("Checking that the new version provided is valid...")
    old_package_version = get_makedeb_package_version(update_build_dir)
    old_pkgver = old_package_version.split("-")[0]
    old_pkgrel = old_package_version.split("-")[1]

    if new_pkgver == old_pkgrel:
        # Check that pkgrel is +1 than the old pkgrel, otherwise throw an error
        if not (new_pkgrel == old_pkgrel + 1):
            logger.error(f"Error updating package {package}! New version is still {new_pkgver}, but pkgrel ({new_pkgrel}) should be {old_pkgrel + 1} instead.")
            return False
    elif new_pkgver != old_pkgrel:
        # TODO: Compare that pkgver has a newer version than old_pkgrel version (maybe with libversion?)
        # Check that pkgrel is 0, otherwise throw an error
        if not new_pkgrel == 0:
            logger.error(f"Error updating package {package} to version {new_pkgver}! pkgrel ({new_pkgrel}) should be 0 for a new version of the package (old version is {old_pkgver}).")
            return False

    logger.info("Done! Writing new release to file...")
    replaced_success = replace_makedeb_package_version(update_build_dir, new_pkgver, new_pkgrel)

    if replaced_success is False:
        logger.error("Error when replacing makedeb package version!")
        return False

    # Build package to verify a successful update
    logger.info(f"Deleting old package directory...")
    subprocess.check_output(f"rm -rf '{package}' && mv '{update_build_dir}' '{package}' > /dev/null", shell=True)
    return build_makedeb_package(user, config, dist, package, suffix, arch, repos, logger, force)


def update_package(debemail, debfullname, package, newversion, user, arch, giturl, dist, suffix, config, repos, logger, force, type_package, upload_repository):
    # Clone package
    clone_package(package, giturl, "")

    # Prepare update build
    update_build_dir = f"{package}-update"
    subprocess.check_output(f"rm -rf {update_build_dir} && cp -r {package} {package}-update", shell=True)

    # TODO: Autodetect package
    # if PKGBUILD is provided = "Makedeb" package
    # if debian folder is provided = "Deb" package

    if type_package == "Deb":
        updated_package = update_package_debian(debemail, debfullname, update_build_dir, package, newversion, user, arch, giturl, dist, suffix, config, repos, logger, force, type_package, upload_repository)
    if type_package == "Makedeb":
        updated_package = update_package_makedeb(update_build_dir, package, newversion, giturl, dist, suffix, config, repos, logger, force, type_package, upload_repository)
    return updated_package

if __name__ == "__main__":
    """
    Arguments:
        --arch: architecture to build package
        --clean: cleans all the chroots, then exits.
        --config: the config file used in obs-build (/usr/lib/obs-build/configs/debian11.conf, for example. It has to match with dist codename). 
        --dist: dist codename (buster, bullseye, stable, oldstable, etc.)
        --name: name of package
        --url-git: URL to git repository
        --force: Forces build even if same version is packaged (boolean)
        --nodbgsym: Do not copy debug symbols on generated packages (boolean)
        --upload-obs: Uploads to OBS (boolean)
        --skip-requirements: Skip Host Requirements (boolean)
    Example:
        * Builder:
            sudo obs-build --repository debian@http://packages.apt.antonialoytorrens.com/debian/bullseye/main --repository debian@http://deb.debian.org/debian/bullseye/main --arch x86_64 --dist debian11 filezilla-obs
            sudo obs-build --repository debian@http://archive.ubuntu.com/ubuntu/jammy/universe --repository debian@http://archive.ubuntu.com/ubuntu/jammy/multiverse --repository debian@http://archive.ubuntu.com/ubuntu/jammy/main --repository debian@http://archive.ubuntu.com/ubuntu/jammy/restricted --arch x86_64 --dist ubuntu2204
            ./obs-builder.py --arch x86_64 --config debian11.conf --dist bullseye --skiprequirements --nodbgsym --giturl https://gitlab.com/aat-debian-repository/debian/mysql-connector-python.git
            ./obs-builder.py --arch x86_64 --config debian11.conf --dist bullseye --skiprequirements --nodbgsym --force --giturl https://salsa.debian.org/sanvila/hello.git
            ./obs-builder.py --arch x86_64 --config debian11.conf --dist bullseye --suffix home --force --giturl https://salsa.debian.org/mozilla-team/firefox.git --suffixdeblocation "security"
            ./obs-builder.py --arch x86_64 --config debian11.conf --dist bullseye --force --giturl https://salsa.debian.org/mozilla-team/firefox.git --suffixdeblocation "security"
            ./obs-builder.py --arch x86_64 --config debian11.conf --dist bullseye --skiprequirements --nodbgsym --exitbuilderror --force --giturl https://gitlab.com/aat-debian-repository/makedeb/libversion.git --typepackage 'Makedeb' --repoorigin 'packages.apt.antonialoytorrens.com' --repolabel 'aat596 APT Repository' --repos 'http://deb.debian.org/debian/bullseye/main'

        * Updater:
            ./obs-builder.py --updatepackage --debemail "example@example.com" --debfullname "Example Builder" --giturl https://salsa.debian.org/sanvila/hello.git --arch x86_64 --config debian11.conf --dist bullseye --newversion "3.10"
            ./obs-builder.py --updatepackage --giturl https://gitlab.com/aat-debian-repository/makedeb/libversion.git --arch x86_64 --config debian11.conf --dist bullseye --typepackage "Makedeb" --newversion "3.10-2"
    """

    # Declare summary_file
    summary_file = Path("summary.log").absolute()

    # Init summary_file
    if not Path(summary_file).exists():
        with open(summary_file, mode="w", encoding="utf-8") as f:
            f.write("")
        # Fix permissions and ownership
        subprocess.check_output(f"sudo chown -R {user}:{user} {summary_file} && sudo chmod -R 644 {summary_file}", shell=True)

    # Declare logger
    logger_init = Logger()

    # Init logger
    logger_init.set_log_filename(f"log/log.txt")
    logger = logger_init.get_logger()

    if not Path("log/log.txt").exists():
        with open("log/log.txt", mode="w", encoding="utf-8") as f:
             f.write("")

    # Check that this script is not running on other instance
    run_builder = False
    while not run_builder:
        lock_file = create_lock_file()
        if lock_file is None:
            logger.error("Another obsbuilder instance is already running. Retrying in five minutes...")
            sleep(300)
        else:
            run_builder = True

    # Get user info
    user = subprocess.check_output(f"whoami", shell=True).decode("utf-8").rstrip()

    aparser = argparse.ArgumentParser(description="Script that builds Debian packages from their Git repository. Note: debian/ subdirectory must be set in Git.")

    # Needed for building / updating packages
    aparser.add_argument("--arch", type=str, help="For which architecture needs the package to be built (x86_64, x86, aarch64, armv7, armhf...).")
    aparser.add_argument("--dist", type=str, help="Distribution codename (buster, bullseye, stable, oldstable, etc.)")
    aparser.add_argument("--config", type=str, help="Config file used in obs-build (inside /usr/lib/obs-build/configs). Example: debian11.conf. It has to match with dist codename.")
    aparser.add_argument("--giturl", type=str, help="URL git repository source")
    aparser.add_argument("--packagename", type=str, help="Package name (Useful if --skipgitcheck is used)")
    aparser.add_argument("--typepackage", type=str, help="Type of package. Accepted values are 'Deb' and 'Makedeb'. Defaults to 'Deb'.")
    # TODO. For now, key has to be imported manually into server
    #aparser.add_argument("--signkey", type=str, help="Set sign key for signing packages")
    aparser.add_argument("--repourl", type=str, help="Set URL repository for signed packages")
    aparser.add_argument("--suffixdeblocation", type=str, help="Add a suffix to the folder where generated deb packages are stored.")
    #aparser.add_argument("--upload-obs", action="store_true", help="Upload to OBS after building package.")
    #aparser.add_argument("--upload-ftp", action="store_true, help="Upload to FTP source after building package.")
    
    # Main and extra repositories
    aparser.add_argument("--repos", type=str, nargs="+", help="Source repositories, including custom ones")

    # Repository information
    aparser.add_argument("--repoorigin", type=str, help="Repository origin URL (packages.example.com, for example)")
    aparser.add_argument("--repolabel", type=str, help="Repository label description (general repository, for example)")

    # Updating packages
    aparser.add_argument("--updatepackage", action="store_true", help="Update packages to latest version and push to Debian repository (giturl needed)")
    aparser.add_argument("--debemail", type=str, help="Set Debian Email. Needed for updating packages.")
    aparser.add_argument("--debfullname", type=str, help="Set Debian Full Name. Needed for updating packages.")
    aparser.add_argument("--newversion", type=str, help="Provide new version of the updated package.")

    # Other options / Miscellaneous options
    aparser.add_argument("--clean", action="store_true", help="Cleans chroot, removes downloaded Debian packages and then exits.")
    aparser.add_argument("--cleanall", action="store_true", help="Cleans gnupg generated keys, chroots, debian packages and repository.")
    aparser.add_argument("--force", action="store_true", help="Forces build even if same version is packaged")
    aparser.add_argument("--nodbgsym", action="store_true", help="Do not copy debug symbols on generated packages (boolean)")
    aparser.add_argument("--skipgitcheck", action="store_true", help="Skip Git repository check (boolean)")
    aparser.add_argument("--installrequirements", action="store_true", help="Install requirements from this script and then exits.")
    aparser.add_argument("--skiprequirements", action="store_true", help="Skip Host requirements if everything is already installed.")
    aparser.add_argument("--exitbuilderror", action="store_true", help="Exit on error if built package has failed.")

    aparser_arguments = aparser.parse_args()

    # Perform aparser checks
    # First will be the ones that exit after invocation

    preserve_file_list = [
        "./TODO",
        "./tmp",
        "./tmp/.gitkeep",
        "./gnupg",
        "./gnupg/gpg.conf",
        "./gnupg/gpg-agent.conf",
        "./log",
        "./log/.gitkeep",
        "./local-apt-repository",
        "./local-apt-repository/rebuild.sh",
        "./local-apt-repository/README.md",
        "./logger.py",
        "./obs-builder.py",
        "./makedeb-builder.py",
        "./obs-builder.py",
        "./run-cronjob.py",
        "./test-makedeb-obs-sample/hello_2.10-3.debian.tar.xz",
        "./test-makedeb-obs-sample/hello_2.10-3.dsc",
        "./test-makedeb-obs-sample/hello_2.10.orig.tar.xz"
    ]

    # CLEAN
    if aparser_arguments.clean:
        clean_build_environment()
        exit(0)

    # CLEANALL
    if aparser_arguments.cleanall:
        cleanall_build_environment(preserve_file_list)
        exit(0)

    # INSTALL REQUIREMENTS
    if aparser_arguments.installrequirements:
        check_host_requirements(logger)
        exit(0)

    # Now the required ones when building a package

    # ARCH
    arch = aparser_arguments.arch
    valid_arches = ["x86_64", "i386", "aarch64", "armhf", "armel", "ppc64le", "s390x"]
    if arch is None or arch not in valid_arches:
        logger.error(f"Arch is empty or not one of: {valid_arches}")
        exit(1)

    # CONFIG
    config = aparser_arguments.config
    if config is None or not Path(f"/usr/lib/obs-build/configs/{config}").exists():
        logger.error(f"Config is empty or non-existing inside /usr/lib/obs-build/configs!")
        exit(1)

    if ".conf" in config:
        config = config[0:-5]

    # DIST
    dist = aparser_arguments.dist
    if dist is None:
        logger.error(f"Distribution is empty!")
        exit(1)

    # SIGNKEY TODO
    """
    signkey = aparser_arguments.signkey
    if signkey is None:
        logger.error(f"Signing key is empty!")
        exit(1)
    """
    signkey = ""

    # REPOURL
    repourl = aparser_arguments.repourl
    #if repourl is None:
    #    logger.error(f"Debian Repository URL is empty!")
    #    exit(1)

    # URL GIT REPOSITORY
    # https://stackoverflow.com/a/7160778 -> validator
    skipgitcheck = False
    if not aparser_arguments.skipgitcheck:
        giturl = parse_giturl(aparser_arguments.giturl)
    else:
        skipgitcheck = True
        giturl = None

    # PACKAGE (get last / from giturl and substract .git)
    if not aparser_arguments.skipgitcheck:
        package = get_package_name(giturl)
    elif aparser_arguments.skipgitcheck and aparser_arguments.packagename:
        package = aparser_arguments.packagename
    else:
        logger.error("Package name (aparser_arguments.packagename) is not provided!")
        exit(1)

    # REPOSITORY SOURCES URL
    if aparser_arguments.repos:
        # FIXME: We accept backports from newer releases to stable
        # Check that distribution matches repository URL (do not accept bad generated repos)
        #for repo in aparser_arguments.repos:
        #    if not str(dist) in str(repo):
        #        logger.error(f"Repository {repo} does not contain distribution {dist}!")
        #        exit(1)

        prefix_repos = " --repository "
        repos = [prefix_repos + repo for repo in aparser_arguments.repos]
        repos = ''.join(repos)
    else:
        logger.error("Missing repository sources!")
        exit(1)

    # REPOSITORY ORIGIN URL
    origin = aparser_arguments.repoorigin
    if origin is None:
        logger.error(f"Repository origin is empty!")
        exit(1)

    # REPOSITORY LABEL DESCRIPTION
    label = aparser_arguments.repolabel
    if label is None:
        logger.error(f"Repository label is empty!")
        exit(1)

    # Check for type of package
    if not aparser_arguments.typepackage:
        type_package = "Deb"
    else:
        type_package = aparser_arguments.typepackage

    # Check for requirements if argument is not provided
    if not aparser_arguments.skiprequirements:
        check_host_requirements(logger)

    # Check for suffix (where generated deb packages are copied)
    if not aparser_arguments.suffixdeblocation:
        suffix = ""
    else:
        suffix = aparser_arguments.suffixdeblocation

    # Check for force_build
    if not aparser_arguments.force:
        force = False
    else:
        force = True

    # Check for nodbgsym
    if not aparser_arguments.nodbgsym:
        nodbgsym = False
    else:
        nodbgsym = True       

    # EXIT ON BUILD ERROR
    if not aparser_arguments.exitbuilderror:
        exitbuilderror = False
    else:
        exitbuilderror = True

    # UPDATE PACKAGES
    updatepackage = aparser_arguments.updatepackage

    # This variable is needed for uploading to setup local deb repository
    updated_package = False

    if updatepackage is not False:
        newversion = None
        debemail = None
        debfullname = None
        if type_package == "Deb":
            if aparser_arguments.debemail is None:
                logger.error(f"Debian Email is empty!")
                exit(1)
            else:
                debemail = aparser_arguments.debemail
            if aparser_arguments.debfullname is None:
                logger.error(f"Debian Full Name is empty!")
                exit(1)
            else:
                debfullname = aparser_arguments.debfullname
        elif type_package == "Makedeb":
            # Makedeb requires newversion provided variable
            if aparser_arguments.newversion is None:
                logger.error("New package version is empty!")
                exit(1)
            else:
                newversion = aparser_arguments.newversion
        updated_package = update_package(debemail, debfullname, package, newversion, user, arch, giturl, dist, suffix, config, repos, logger, force, type_package, "")

    # Build the package in Chroot if we do not update
    built_package = False

    if not updatepackage:
        built_package = build_package(package, user, arch, giturl, dist, suffix, config, repos, logger, force_build=force, skip_git_check=skipgitcheck, type_package=type_package)

    version = get_package_version(package)

    # Generate local repository with all the package information, if package has been built
    # Then, clean everything
    if built_package is True or updated_package is True:
        generate_simple_local_deb_repository(arch, user, config, dist, nodbgsym, suffix, logger)
        sign_deb_packages(arch, user, config, dist, signkey, suffix, logger)
        setup_deb_repository(package, user, config, dist, repourl, origin, label, suffix, logger, force_build=force)

        logger.info(f"Cleaning package '{package}' folders...")
        clean_build_environment()
        # query_arch_all(package, arch) reports x86_64 because folder does not exist for checking
        subprocess.check_output(f"sudo rm -rf {package} && sudo rm -rf {package}-obs && sudo rm -rf {package}-update", shell=True)
        #subprocess.check_output(f"sudo rm -rf {package}-obs", shell=True)

    # Write report if not existent
    # FIXME: f"{built_package}" may be BUILT and then SKIP due to being a Python package: it is only built one time!
    built_package = query_built_status(built_package)
    arch = query_arch_all(package, arch)
    report_new = dumps({f"package":[f"{package}", f"{arch}", f"{version}", f"{config}", f"{dist}", f"{built_package}"]})

    # Workaround
    report_built = dumps({f"package":[f"{package}", f"{arch}", f"{version}", f"{config}", f"{dist}", "BUILT"]})
    report_skip = dumps({f"package":[f"{package}", f"{arch}", f"{version}", f"{config}", f"{dist}", "SKIP"]})

    logger.info("Writing report...")
    found_report = False

    with open(summary_file, mode="a+", encoding="utf-8") as f:
        f.seek(0)
        data = f.readlines()
        if not data:
            f.write(report_new)
            f.write("\n")
        else:
            for report in data:
                # Loop through each report of data and compare
                if report_new in report.strip() or report_built in report.strip() or report_skip in report.strip():
                    logger.warning(f"Summary file already contains package {package}, version {version}, {config} ({dist}) {arch}.")
                    found_report = True
                    break

            if not found_report:
                f.write(report_new)
                f.write("\n")

    logger.info("Done!")

    # Builder has finished, close lock file
    lock_file_path = get_lock_filepath()
    logger.info("Removing lock file...")
    lock_file.close()
    remove(lock_file_path)

    # Exit on error if built_package failed
    if built_package == "FAILED" and exitbuilderror:
       logger.error("Exit due to build errors!")
       exit(1)
