#!/usr/bin/env python3
# This script must be run as root
# Put this in /etc/cron.daily/debuilder-run-check

import subprocess
import traceback
from logger import Logger
from datetime import datetime
from pathlib import Path
from shutil import move, rmtree

# Declare constants
USER = "obsbuilder"
DATETIME_TODAY = datetime.today().strftime("%Y-%m-%d")
GITURL_DEBIAN = "https://gitlab.com/aat-debian-repository/debian"
GITURL_MAKEDEB = "https://gitlab.com/aat-debian-repository/makedeb"
LOG_DIRECTORY = "/var/log/debuilder-run-check"
PENDING_DIRECTORY = "/var/log/debuilder-run-check/pending"
DONE_DIRECTORY = "/var/log/debuilder-run-check/done"
#ARCHES = ["x86_64", "i386", "aarch64", "armhf", "armel"]
ARCHES = ["x86_64"]
DEB_RELEASES = ["debian11.conf bullseye"]
DEB_BACKPORT_RELEASES = ["debian12.conf bullseye"]
SUMMARY = "summary.log"

# Packages
PACKAGES_DEBIAN=[
    # Linux Mint collection and deps
    "xapp",
    "mintinstall",
    "webapp-manager",
    "debian-system-adjustments",
    "mintupdate",
    "hypnotix",
    "mintstick",

    # Kdenlive tweak deps
    "purpose",

    # GIMP
    "babl",
    "gegl",
    "gimp",

    # LGames section
    "lbreakouthd",
    "lpairs2",

    # SRB2Kart
    "srb2kart-data-downloader",
    "srb2kart",

    # SRB2
    "srb2-data-downloader",
    "srb2",

    # Chromium
    # Commented because it has its own cronjob
    # and runs from apt-get source
    "openh264",
    #"chromium",

    # SuperTuxKart
    # Commented because it has its own cronjob
    # and runs from apt-get source
    #"supertuxkart",

    # MySQL workbench deps
    "mysql-connector-python",
    "mysql-workbench",

    # Blender LTS
    #"blender",

    # Virtualbox section
    "virtualbox-ext-pack",
    "virtualbox-guest-additions-iso",
    "virtualbox",

    # ElementaryOS code deps
    "granite6",
    "io.elementaryos.code",

    # Filezilla deps
    # Commented because it has its own cronjob
    #"libfilezilla",
    #"filezilla",

    # Prismlauncher deps
    "tomlplusplus",
    "libghc-filesystem",
    "libquazip1-qt5",
    "prismlauncher",

    # PostmarketOS Wiki doc
    # Commented because it has its own cronjob
    # "postmarketos-wiki-doc",

    # OpenLens deps
    "node-openlens-pod-menu",

    # Yuzu deps (some of them are bundled)
    "fmtlib",
    "yuzu",

    # GNOME shell extensions
    "gnome-shell-extension-vitals",
    "gnome-shell-extension-cpupower",
    "gnome-shell-extensions-extra",

    # Python packages
    "python-simplemediawiki",

    # Misc tweaks
    "chromium-tweaks",
    "dbeaver-ce-tweaks",
    "synaptic-tweaks",
    "gnome-shell-extension-weather-tweaks",

    # Packages with bundled deps
    "citra",

    # Temporary: cpupower-gui needs meson from bullseye-backports
    "meson",
    "cpupower-gui",

    # Temporary: bluez 5.68 is needed in order to support PS5 controller
    # bluez 5.69 and 5.70 are broken versions. 5.71 would be ideal
    "ell",
    "bluez",

    # Packages without deps
    "kdbg",
    "evolution-indicator",
    "notepadqq",
    "musescore3",
    "musescore4",
    "pipe-viewer",
    "grandorgue",
    "meta-gnome3",
    "pitivi",
    "bottles",
    "reaper-downloader",
    "gnome-info-collect",
    "input-leap",
    "linssid",
    "woeusb-ng",
    "phantomjs",
    "schism",
    "songrec",
    "watercloset",
    "fxos-browser-simulator"
]

PACKAGES_DEBIAN_TESTING=[
    # Needs npm
    "gnome-shell-extension-clipboard",

    # Needs Debian Bookworm
    "passbolt-browser",
    "full-page-screen-capture",
    "marvellous-suspender",
    "savescreenshot",
    "javascript-toggle-on-and-off",
    "return-youtube-dislike",
    "translate-web-pages",
    "fx-private-relay-add-on",
    "dark-reader",
    "snowflake",
    "eye-dropper"
]

PACKAGES_MAKEDEB=[
    # Itch packages
    "itch-setup",
    "itch",

    "pycharm-community",
    "python3.10",
    "python3.11",
    "meowsql",
    "fractal",
    "clockify-desktop",
    "clickup",
    "postman",
    #"onivim2", # Not in use anymore
    "libversion",
    "gamejolt",
    "lagrange",
    "castor-gtk",
    "drawio-desktop",
    "electronmail",
    #"screamingfrogseospider", # Not redistributable
    "streamlink-twitch-gui",
    "docker-compose",
    "openlens",
    "geckodriver",
    "chatterino2",
    "protonup-qt",
    "ruffle"
]

PACKAGES_DEBIAN = []
PACKAGES_DEBIAN_TESTING = []
#PACKAGES_MAKEDEB = []

# Create log directory
Path(LOG_DIRECTORY).mkdir(exist_ok=True)

# Create (or recreate) pending directory
rmtree(PENDING_DIRECTORY, ignore_errors=True)
Path(PENDING_DIRECTORY).mkdir(exist_ok=True)

# Create (or recreate) done directory
rmtree(DONE_DIRECTORY, ignore_errors=True)
Path(DONE_DIRECTORY).mkdir(exist_ok=True)

# Declare logger
logger_init = Logger()

# Init logger
logger_init.set_log_filename(f"{LOG_DIRECTORY}/debuilder-run.log")
logger = logger_init.get_logger()

# Declare summary_file
summary_file = Path(SUMMARY).absolute()

# Init summary_file
if not Path(summary_file).exists():
    with open(summary_file, mode="w", encoding="utf-8") as f:
        f.write("")

# Check host requirements
subprocess.check_output(f"sudo runuser -l {USER} -c \"python3 obs-builder.py --installrequirements\"", shell=True, timeout=172800)

# Add all packages with release codename and all arches into pending directory
# obs-builder.py will always be executed: this script already checks if the package has been built or not
for debpackage in PACKAGES_DEBIAN:
    for debrelease in DEB_RELEASES:
        dist = debrelease.split(" ")[1]
        for arch in ARCHES:
            pending_package = f"{PENDING_DIRECTORY}/{dist}/{arch}"
            Path(pending_package).mkdir(parents=True, exist_ok=True)
            with open(f"{pending_package}/{debpackage}.txt", mode="w") as f:
                f.write("")

for debpackage in PACKAGES_DEBIAN:
    for debrelease in DEB_RELEASES:
        configfile = debrelease.split(" ")[0]
        dist = debrelease.split(" ")[1]
        for arch in ARCHES:
            try:
                logger.info(f"STARTED BUILD FOR '{debpackage}', CONFIG {configfile}, DISTRIBUTION {dist}")
                subprocess.check_output(f"sudo runuser -l {USER} -c \"/usr/bin/build-debian-package '{GITURL_DEBIAN}/{debpackage}.git'\"", shell=True)
                logger.info(f"ENDED BUILD FOR '{debpackage}', CONFIG {configfile}, DISTRIBUTION {dist}")
            except Exception as e:
                logger.error(f"Error occurred in package {debpackage}, arch: {arch}, dist: {dist}!")
                logger.error(f"Exception occurred!\nException: {e}\nTraceback: {traceback.print_exc()}")

            # Done! Move into done destination
            pending_package = f"{PENDING_DIRECTORY}/{dist}/{arch}"
            done_package = f"{DONE_DIRECTORY}/{dist}/{arch}"
            Path(done_package).mkdir(parents=True, exist_ok=True)
            move(f"{pending_package}/{debpackage}.txt", f"{done_package}/{debpackage}.txt")

# PACKAGES THAT NEED DEBIAN TESTING TO BUILD BUT CAN WORK ON STABLE RELEASE
for debpackage in PACKAGES_DEBIAN_TESTING:
    for debrelease in DEB_BACKPORT_RELEASES:
        configfile = debrelease.split(" ")[0]
        dist = debrelease.split(" ")[1]
        #for arch in ARCHES: # Only extensions for now, no need to separate between arches
        arch = "x86_64"
        try:
            # Timeout: 16h
            logger.info(f"STARTED BUILD FOR '{debpackage}', CONFIG {configfile}, DISTRIBUTION {dist}")
            logger.debug(f"Running command: sudo runuser -l {USER} -c \"/usr/bin/build-backport-debian-testing-stable-package '{GITURL_DEBIAN}/{debpackage}.git'\"")
            subprocess.check_output(f"sudo runuser -l {USER} -c \"/usr/bin/build-backport-debian-testing-stable-package '{GITURL_DEBIAN}/{debpackage}.git'\"", shell=True)
            logger.info(f"ENDED BUILD FOR '{debpackage}', CONFIG {configfile}, DISTRIBUTION {dist}")
        except Exception as e:
            logger.error(f"Error occurred in package {debpackage}, arch: {arch}, dist: {dist}!")
            logger.error(f"Exception occurred!\nException: {e}\nTraceback: {traceback.print_exc()}")

        # Done! Move into done destination
        pending_package = f"{PENDING_DIRECTORY}/{dist}/{arch}"
        done_package = f"{DONE_DIRECTORY}/{dist}/{arch}"
        Path(done_package).mkdir(parents=True, exist_ok=True)
        try:
            move(f"{pending_package}/{debpackage}.txt", f"{done_package}/{debpackage}.txt")
        except FileNotFoundError:
            pass

# TODO: makedeb packages. But better if we package them as Debian packages
for makedebpackage in PACKAGES_MAKEDEB:
    for debrelease in DEB_RELEASES:
        configfile = debrelease.split(" ")[0]
        dist = debrelease.split(" ")[1]
        #for arch in ARCHES: # Build makedeb package regardless of release and arch (x86_64 supported only, legacy support)
        arch = "x86_64"
        try:
            # Timeout: 16h
            logger.info(f"Running command: sudo runuser -l {USER} -c \"/usr/bin/build-makedeb-package '{GITURL_MAKEDEB}/{makedebpackage}.git'\"")
            status = subprocess.check_output(f"sudo runuser -l {USER} -c \"/usr/bin/build-makedeb-package '{GITURL_MAKEDEB}/{makedebpackage}.git'\"", shell=True)
        except Exception as e:
            logger.error(f"Error occurred in package {makedebpackage}!")
            logger.error(f"Exception occurred!\nException: {e}\nTraceback: {traceback.print_exc()}")

# Finally, show summary
# We import libs here because we need to install deps first
from summary import *
show_summary(USER, SUMMARY, logger)
